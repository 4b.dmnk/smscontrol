package nyc.artdecoders.smscontrol.data;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Created by Dominik on 09.02.2018.
 */

public class SMSControlProvider extends ContentProvider {

    public static final int CODE_DEVICE = 100;
    public static final int CODE_DEVICE_WITH_ID = 101;
    public static final int CODE_DEVICE_FILTER = 102;

    public static final int CODE_CONFIGURED_DEVICE = 200;
    public static final int CODE_CONFIGURED_DEVICE_WITH_ID = 201;

    public static final int CODE_ACTIONSFORCONFIGURED_DEVICE = 300;
    public static final int CODE_ACTIONSFORCONFIGURED_DEVICE_WITH_ID = 301;

    public static final int CODE_CONFIGURED_DEVICES_DEEP = 400;
    public static final int CODE_CONFIGURED_DEVICES_DEEP_WITH_ID = 401;

    private static final UriMatcher sUriMatcher = buildUriMatcher();
    private SMSControlDbHelper mOpenHelper;

    private static UriMatcher buildUriMatcher() {

        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        final String authority = SMSControlContract.CONTENT_AUTHORITY;

        matcher.addURI(authority, SMSControlContract.PATH_DEVICES, CODE_DEVICE);
        matcher.addURI(authority, SMSControlContract.PATH_DEVICES + "/#", CODE_DEVICE_WITH_ID);
        matcher.addURI(authority, SMSControlContract.PATH_DEVICES_FILTER + "/*", CODE_DEVICE_FILTER);

        matcher.addURI(authority, SMSControlContract.PATH_CONFIGURED_DEVICES, CODE_CONFIGURED_DEVICE);
        matcher.addURI(authority, SMSControlContract.PATH_CONFIGURED_DEVICES + "/#", CODE_CONFIGURED_DEVICE_WITH_ID);

        matcher.addURI(authority, SMSControlContract.PATH_ACTIONS_CONFIGURED_DEVICES, CODE_ACTIONSFORCONFIGURED_DEVICE);
        matcher.addURI(authority, SMSControlContract.PATH_ACTIONS_CONFIGURED_DEVICES + "/#", CODE_ACTIONSFORCONFIGURED_DEVICE_WITH_ID);

        matcher.addURI(authority, SMSControlContract.PATH_CONFIGURED_DEVICES_DEEP, CODE_CONFIGURED_DEVICES_DEEP);
        matcher.addURI(authority, SMSControlContract.PATH_CONFIGURED_DEVICES_DEEP + "/#", CODE_CONFIGURED_DEVICES_DEEP_WITH_ID);

        return matcher;
    }

    @Override
    public boolean onCreate() {
        mOpenHelper = new SMSControlDbHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        Cursor cursor;

        switch (sUriMatcher.match(uri)) {

            case CODE_DEVICE_WITH_ID: {

                String normalizedUtcDateString = uri.getLastPathSegment();

                String[] selectionArguments = new String[]{normalizedUtcDateString};

                cursor = mOpenHelper.getReadableDatabase().query(
                        /* Table we are going to query */
                        SMSControlContract.DeviceEntry.TABLE_NAME,
                        projection,
                        SMSControlContract.DeviceEntry._ID + " = ? ",
                        selectionArguments,
                        null,
                        null,
                        sortOrder);

                break;
            }
            case CODE_DEVICE: {
                cursor = mOpenHelper.getReadableDatabase().query(
                        SMSControlContract.DeviceEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);

                break;
            }
            case CODE_CONFIGURED_DEVICE_WITH_ID: {

                String normalizedUtcDateString = uri.getLastPathSegment();

                String[] selectionArguments = new String[]{normalizedUtcDateString};

                cursor = mOpenHelper.getReadableDatabase().query(
                        /* Table we are going to query */
                        SMSControlContract.ConfiguredDeviceEntry.TABLE_NAME,
                        projection,
                        SMSControlContract.ConfiguredDeviceEntry._ID + " = ? ",
                        selectionArguments,
                        null,
                        null,
                        sortOrder);

                break;
            }
            case CODE_DEVICE_FILTER: {
                String idFilt = uri.getPathSegments().get(1);
                String mSelection = SMSControlContract.DeviceEntry.COLUMN_NAME_NAME + " LIKE '%' || ? || '%' " +
                        " OR " + SMSControlContract.DeviceEntry.COLUMN_NAME_PRODUCER + " LIKE '%' || ? || '%' ";/* +
                        " OR " + PoetcomContract.MachineReportSummaryEntry.COLUMN_CHASSISNR + " LIKE '%' || ? || '%' ";*/
                String[] mSelectionArgs = new String[]{idFilt, idFilt};
                cursor = mOpenHelper.getReadableDatabase().query(SMSControlContract.DeviceEntry.TABLE_NAME,
                        projection,
                        mSelection,
                        mSelectionArgs,
                        null,
                        null,
                        null);
                break;
            }
            case CODE_CONFIGURED_DEVICE: {
                cursor = mOpenHelper.getReadableDatabase().query(
                        SMSControlContract.ConfiguredDeviceEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);

                break;
            }

            case CODE_ACTIONSFORCONFIGURED_DEVICE_WITH_ID:
                String id = uri.getLastPathSegment();
                selection = new String (SMSControlContract.ActionsForConfiguredDeviceEntry.COLUMN_NAME_CONFIGUREDDEVICE_ID + " = ?");
                selectionArgs = new String[]{id};

                cursor = mOpenHelper.getReadableDatabase().query(
                        SMSControlContract.ActionsForConfiguredDeviceEntry.VIEW_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                break;


            case CODE_CONFIGURED_DEVICES_DEEP: {
                cursor = mOpenHelper.getReadableDatabase().query(
                        SMSControlContract.ConfiguredDeviceDeepEntry.VIEW_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);

                break;
            }

            case CODE_CONFIGURED_DEVICES_DEEP_WITH_ID:
                String mid = uri.getLastPathSegment();
                selection = new String (SMSControlContract.ConfiguredDeviceDeepEntry._ID + " = ?");
                selectionArgs = new String[]{mid};

                cursor = mOpenHelper.getReadableDatabase().query(
                        /* Table we are going to query */
                        SMSControlContract.ConfiguredDeviceDeepEntry.VIEW_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                break;

            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues contentValues) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        int match = sUriMatcher.match(uri);

        Uri returnUri;
        long id;

        switch(match) {
            //Can insert just in directories!
            case CODE_CONFIGURED_DEVICE:
                id = db.insert(SMSControlContract.ConfiguredDeviceEntry.TABLE_NAME, null, contentValues);
                if(id > 0) {
                    returnUri = ContentUris.withAppendedId(SMSControlContract.ConfiguredDeviceEntry.CONTENT_URI, id);
                } else {
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                }
                break;
            default:
                throw new UnsupportedOperationException("Not yet implemented");
        }

        getContext().getContentResolver().notifyChange(uri, null);
        getContext().getContentResolver().notifyChange(SMSControlContract.ConfiguredDeviceDeepEntry.CONTENT_URI, null);
        return returnUri;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        int numRowsDeleted;

        if (null == selection) selection = "1";

        switch (sUriMatcher.match(uri)) {
            case CODE_DEVICE:
                numRowsDeleted = mOpenHelper.getWritableDatabase().delete(
                        SMSControlContract.DeviceEntry.TABLE_NAME,
                        selection,
                        selectionArgs);
                break;

            case CODE_CONFIGURED_DEVICE_WITH_ID:
                String id = uri.getLastPathSegment().toString();

                selection = SMSControlContract.ConfiguredDeviceEntry._ID + " = ? ";
                selectionArgs = new String[]{id};

                numRowsDeleted = mOpenHelper.getWritableDatabase().delete(
                        SMSControlContract.ConfiguredDeviceEntry.TABLE_NAME,
                        selection,
                        selectionArgs);
                break;

            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        if (numRowsDeleted != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return numRowsDeleted;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues contentValues, @Nullable String s, @Nullable String[] strings) {
        return 0;
    }

    @Override
    public int bulkInsert(@NonNull Uri uri, @NonNull ContentValues[] values) {

        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();

        switch (sUriMatcher.match(uri)) {

            case CODE_DEVICE:
                db.beginTransaction();
                int rowsInserted = 0;
                try {
                    for (ContentValues value : values) {
                        long _id = db.insert(SMSControlContract.DeviceEntry.TABLE_NAME, null, value);
                        if (_id != -1) {
                            rowsInserted++;
                        }
                    }
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }

                if (rowsInserted > 0) {
                    getContext().getContentResolver().notifyChange(uri, null);
                }

                return rowsInserted;

            default:
                return super.bulkInsert(uri, values);
        }
    }

    public void shutdown() {
        mOpenHelper.close();
        super.shutdown();
    }
}

