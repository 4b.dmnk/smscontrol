package nyc.artdecoders.smscontrol.data;

import android.content.res.Resources;
import android.net.Uri;
import android.provider.BaseColumns;

import nyc.artdecoders.smscontrol.R;

/**
 * Created by Dominik on 31.01.2018.
 */

public class SMSControlContract {

    public static final String CONTENT_AUTHORITY = "nyc.artdecoders.smscontrol";

    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    public static final String PATH_DEVICES = "devices";
    public static final String PATH_DEVICES_FILTER = "devices_filter";
    public static final String PATH_CONFIGURED_DEVICES = "configured_devices";
    public static final String PATH_ACTIONS_CONFIGURED_DEVICES = "actions_configured_devices";
    public static final String PATH_CONFIGURED_DEVICES_DEEP = "configured_devices_deep";


    private SMSControlContract() {}

    public static class DeviceEntry implements BaseColumns {
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon()
            .appendPath(PATH_DEVICES)
            .build();

        public static final Uri CONTENT_FILTER_URI =
                BASE_CONTENT_URI.buildUpon().appendEncodedPath(PATH_DEVICES_FILTER).build();

        public static final String TABLE_NAME = "device";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_TYPE_ID = "typeID";
        public static final String COLUMN_NAME_PRODUCER = "producer";
    }

    public static class ActionEntry implements BaseColumns {
        public static final String TABLE_NAME = "action";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_TRANSLAT_KEY = "trans_key";
        public static final String COLUMN_NAME_ICON = "icon";
    }

    public static class ConfiguredDeviceEntry implements BaseColumns {
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon()
                .appendPath(PATH_CONFIGURED_DEVICES)
                .build();

        public static final String TABLE_NAME = "ConfiguredDevice";
        public static final String COLUMN_NAME_DESCRIPTION = "description";
        public static final String COLUMN_NAME_PHONENUMBER = "phonenumber";
        public static final String COLUMN_NAME_PASSWORD = "password";
        public static final String COLUMN_NAME_DEVICE_ID = "deviceID";
        public static final String COLUMN_NAME_CUSTOM_BACKGROUND = "background_path";
        public static final String COLUMN_NAME_CUSTOM_IMAGE = "image_path";
        public static final String COLUMN_NAME_COLOR = "color";

    }

    public static class ConfiguredDeviceDeepEntry implements BaseColumns {
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon()
                .appendPath(PATH_CONFIGURED_DEVICES_DEEP)
                .build();

        public static final String VIEW_NAME = "v_ConfiguredDevice_references";
        public static final String COLUMN_NAME_DESCRIPTION = "description";
        public static final String COLUMN_NAME_PHONENUMBER = "phonenumber";
        public static final String COLUMN_NAME_PASSWORD = "password";
        public static final String COLUMN_NAME_DEVICE_ID = "deviceID";
        public static final String COLUMN_NAME_CUSTOM_BACKGROUND = "background_path";
        public static final String COLUMN_NAME_CUSTOM_IMAGE = "image_path";
        public static final String COLUMN_NAME_COLOR = "color";
        public static final String COLUMN_NAME_TYPE_ID = "typeID";

    }

    public static class TypeEntry implements BaseColumns {
        public static final String TABLE_NAME = "type";
        public static final String COLUMN_NAME_NAME = "name";
    }

    public static class DeviceActionEntry implements BaseColumns {
        public static final String TABLE_NAME = "deviceaction";
        public static final String COLUMN_NAME_DEVICE_ID = "deviceID";
        public static final String COLUMN_NAME_ACTION_ID = "actionID";
        public static final String COLUMN_NAME_COMMAND = "command";
    }

    public static class ActionsForConfiguredDeviceEntry implements BaseColumns {
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon()
                        .appendPath(PATH_ACTIONS_CONFIGURED_DEVICES)
                        .build();

        public static final String VIEW_NAME = "v_actionsfordevice";
        public static final String COLUMN_NAME_CONFIGUREDDEVICE_ID = "configureddeviceID";
        public static final String COLUMN_NAME_ACTION_ID = "actionID";
        public static final String COLUMN_NAME_COMMAND = "command";
        public static final String COLUMN_NAME_NAME = "action_name";
        public static final String COLUMN_NAME_ACTION_TRANSLATION_KEY = "action_translation";
        public static final String COLUMN_NAME_ACTION_ICON = "action_icon";
    }

}
