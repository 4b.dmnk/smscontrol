package nyc.artdecoders.smscontrol.data;

import android.bluetooth.BluetoothClass;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import nyc.artdecoders.smscontrol.R;

import nyc.artdecoders.smscontrol.ConfiguredDevice;
import nyc.artdecoders.smscontrol.data.SMSControlContract.*;

/**
 * Created by Dominik on 27.01.2018.
 */

public class SMSControlDbHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "smscontrol.db";
    private static final int DATABASE_VERSION = 1;

    public SMSControlDbHelper(Context c) {
        super(c, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        final String SQL_CREATE_DEVICE_TABLE = "CREATE TABLE "+
                DeviceEntry.TABLE_NAME + " ("+
                DeviceEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "+
                DeviceEntry.COLUMN_NAME_NAME + " TEXT, "+
                DeviceEntry.COLUMN_NAME_PRODUCER + " TEXT, "+
                DeviceEntry.COLUMN_NAME_TYPE_ID + " INTEGER NOT NULL"+
                ");";

        //TODO: put the name in the strings.xml and use the key here
        // but, that would add a dependency between the xml and the DB
        // dont like this
        final String SQL_CREATE_TYPE_TABLE = "CREATE TABLE "+
                TypeEntry.TABLE_NAME + " ("+
                TypeEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TypeEntry.COLUMN_NAME_NAME + " TEXT "+
                ");";

        final String SQL_CREATE_DEVICE_ACTION_TABLE =" CREATE TABLE "+
                DeviceActionEntry.TABLE_NAME + " ("+
                //DeviceActionEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "+
                DeviceActionEntry.COLUMN_NAME_ACTION_ID + " INTEGER, "+
                DeviceActionEntry.COLUMN_NAME_DEVICE_ID + " INTEGER, "+
                DeviceActionEntry.COLUMN_NAME_COMMAND + " TEXT"+
                ");";

        final String SQL_CREATE_ACTION_TABLE = "CREATE TABLE "+
                ActionEntry.TABLE_NAME + " ("+
                ActionEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "+
                ActionEntry.COLUMN_NAME_NAME + " TEXT, "+
                ActionEntry.COLUMN_NAME_TRANSLAT_KEY + " TEXT, "+
                ActionEntry.COLUMN_NAME_ICON + " TEXT "+
                ");";

        final String SQL_CREATE_CONFIGUREDDEVICE_TABLE = "CREATE TABLE "+
                ConfiguredDeviceEntry.TABLE_NAME + " ("+
                ConfiguredDeviceEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "+
                ConfiguredDeviceEntry.COLUMN_NAME_DEVICE_ID + " INTEGER, "+
                ConfiguredDeviceEntry.COLUMN_NAME_DESCRIPTION + " TEXT, "+
                ConfiguredDeviceEntry.COLUMN_NAME_PHONENUMBER + " TEXT, "+
                ConfiguredDeviceEntry.COLUMN_NAME_PASSWORD + " TEXT, "+
                ConfiguredDeviceEntry.COLUMN_NAME_CUSTOM_BACKGROUND + " TEXT, "+
                ConfiguredDeviceEntry.COLUMN_NAME_CUSTOM_IMAGE + " TEXT, " +
                ConfiguredDeviceEntry.COLUMN_NAME_COLOR + " TEXT " +
                ");";

        final String SQL_CREATE_ACTIONSFORCONFIGUREDDEVICE_VIEW = "CREATE VIEW "+ ActionsForConfiguredDeviceEntry.VIEW_NAME +" AS "+
                "SELECT " + "a."+ActionEntry.COLUMN_NAME_NAME + " AS " + ActionsForConfiguredDeviceEntry.COLUMN_NAME_NAME +", " +
                "a." + ActionEntry.COLUMN_NAME_TRANSLAT_KEY + " AS " + ActionsForConfiguredDeviceEntry.COLUMN_NAME_ACTION_TRANSLATION_KEY +", "+
                "a." + ActionEntry.COLUMN_NAME_ICON + " AS " + ActionsForConfiguredDeviceEntry.COLUMN_NAME_ACTION_ICON +", "+
                "a." + ActionEntry._ID + " AS " + ActionsForConfiguredDeviceEntry.COLUMN_NAME_ACTION_ID + ", "+
                "da." + DeviceActionEntry.COLUMN_NAME_COMMAND + " AS " + ActionsForConfiguredDeviceEntry.COLUMN_NAME_COMMAND + ", " +
                "c." + ConfiguredDeviceEntry._ID + " AS " + ActionsForConfiguredDeviceEntry.COLUMN_NAME_CONFIGUREDDEVICE_ID +
                " FROM " + ConfiguredDeviceEntry.TABLE_NAME + " AS " + " c " +
                "LEFT JOIN " + DeviceEntry.TABLE_NAME + " AS " + "de" +
                    " ON " + "de." + DeviceEntry._ID + " = " + "c." + ConfiguredDeviceEntry.COLUMN_NAME_DEVICE_ID +
                " LEFT JOIN " + DeviceActionEntry.TABLE_NAME + " AS " + "da" +
                    " ON " + "da." + DeviceActionEntry.COLUMN_NAME_DEVICE_ID + " = " + "de."+ DeviceEntry._ID +
                " LEFT JOIN " + ActionEntry.TABLE_NAME + " AS " + " a " +
                    " ON " + "a."+ ActionEntry._ID + " = " + "da." + DeviceActionEntry.COLUMN_NAME_ACTION_ID
                +";";

        final String SQL_CREATE_CONFIGUREDDEVICES_DEEP_VIEW = "CREATE VIEW "+ ConfiguredDeviceDeepEntry.VIEW_NAME + " AS " +
                " SELECT " + "d." + ConfiguredDeviceEntry._ID + " AS " + ConfiguredDeviceDeepEntry._ID + ", " +
                "d." + ConfiguredDeviceEntry.COLUMN_NAME_DESCRIPTION + " AS " + ConfiguredDeviceDeepEntry.COLUMN_NAME_DESCRIPTION + ", " +
                "d." + ConfiguredDeviceEntry.COLUMN_NAME_COLOR + " AS " + ConfiguredDeviceDeepEntry.COLUMN_NAME_COLOR + ", " +
                "d." + ConfiguredDeviceEntry.COLUMN_NAME_CUSTOM_BACKGROUND + " AS " + ConfiguredDeviceDeepEntry.COLUMN_NAME_CUSTOM_BACKGROUND + ", " +
                "d." + ConfiguredDeviceEntry.COLUMN_NAME_CUSTOM_IMAGE + " AS " + ConfiguredDeviceDeepEntry.COLUMN_NAME_CUSTOM_IMAGE + ", " +
                "d." + ConfiguredDeviceEntry.COLUMN_NAME_DEVICE_ID + " AS " + ConfiguredDeviceDeepEntry.COLUMN_NAME_DEVICE_ID + ", " +
                "de." + DeviceEntry.COLUMN_NAME_TYPE_ID + " AS " + ConfiguredDeviceDeepEntry.COLUMN_NAME_TYPE_ID + ", "+
                "d." + ConfiguredDeviceEntry.COLUMN_NAME_PASSWORD + " AS " + ConfiguredDeviceDeepEntry.COLUMN_NAME_PASSWORD + ", "  +
                "d." + ConfiguredDeviceEntry.COLUMN_NAME_PHONENUMBER + " AS " + ConfiguredDeviceDeepEntry.COLUMN_NAME_PHONENUMBER + " "+
                "FROM " + ConfiguredDeviceEntry.TABLE_NAME + " AS " + " d " +
                "LEFT JOIN " + DeviceEntry.TABLE_NAME + " AS " + " de " +
                    " ON " + "d." + ConfiguredDeviceEntry.COLUMN_NAME_DEVICE_ID + " = " + "de." + DeviceEntry._ID +
                /*"LEFT JOIN " + TypeEntry.TABLE_NAME + " AS " + "t" +
                    " ON " + "t." + TypeEntry._ID*/
                ";";


        sqLiteDatabase.execSQL(SQL_CREATE_DEVICE_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_TYPE_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_DEVICE_ACTION_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_ACTION_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_CONFIGUREDDEVICE_TABLE);

        sqLiteDatabase.execSQL(SQL_CREATE_ACTIONSFORCONFIGUREDDEVICE_VIEW);
        sqLiteDatabase.execSQL(SQL_CREATE_CONFIGUREDDEVICES_DEEP_VIEW);

        insertTypes(sqLiteDatabase);
        insertDevices(sqLiteDatabase);
        insertActions(sqLiteDatabase);
        insertDevicesActions(sqLiteDatabase);
    }

    private void insertTypes(SQLiteDatabase db) {
        //TODO: jamie oliver bisquit tiramisu
        final Map<Integer, String> typeList = new HashMap<Integer, String>();
        typeList.put(1, "Home Security");
        typeList.put(2, "Power Socket");
        typeList.put(3, "Heating");
        typeList.put(4, "GPS");
        typeList.put(5, "Video Surveillance");

        Iterator it = typeList.entrySet().iterator();
        ContentValues cv;

        while(it.hasNext()) {
            Map.Entry e = (Map.Entry)it.next();

            cv = new ContentValues();
            cv.put(TypeEntry._ID, (Integer) e.getKey());
            cv.put(TypeEntry.COLUMN_NAME_NAME, (String) e.getValue());
            db.insert(TypeEntry.TABLE_NAME, null, cv);
        }
    }

    private void insertActions(SQLiteDatabase db) {
        //TODO: jamie oliver bisquit tiramisu
        final Map<Integer, String> actionList = new HashMap<Integer, String>();

        actionList.put(9, "Arm");
        actionList.put(14, "Capture");
        actionList.put(15, "Capture-Mail");
        actionList.put(20, "Disarm");
        actionList.put(13, "IR-Disable");
        actionList.put(12, "IR-Enable");
        actionList.put(19, "Status");
        actionList.put(10, "Time-Arm");
        actionList.put(11, "Time-Disarm");
        actionList.put(8, "Locate");
        actionList.put(6, "Off");
        actionList.put(5, "On");
        actionList.put(16, "Status");
        actionList.put(7, "Toggle");
        actionList.put(2, "Arm-Away");
        actionList.put(3, "Arm-Stay");
        actionList.put(4, "Disarm");
        actionList.put(36, "Set Disarm Password");
        actionList.put(23, "Get Alerted-Numbers");
        actionList.put(32, "Get Alert Level and Length");
        actionList.put(26, "Get Alert-Sound-Length");
        actionList.put(25, "Get Alert-Sound-Level");
        actionList.put(24, "Get Entry-Delay");
        actionList.put(27, "Get Commands");
        actionList.put(37, "Get Commands(2)");
        actionList.put(38, "Get Commands(3)");
        actionList.put(30, "Set Language English");
        actionList.put(29, "Set Language French");
        actionList.put(28, "Set Language German");
        actionList.put(22, "Microphone-Callback");
        actionList.put(21, "Quit-Alarm");
        actionList.put(35, "Get RFID-Notification List");
        actionList.put(33, "Get RFID-SMS Number");
        actionList.put(34, "Get Speeddial");
        actionList.put(1, "Status");
        actionList.put(31, "2 Way Audio-Callback");
        actionList.put(18, "Off");
        actionList.put(17, "On");
        actionList.put(39, "Arm-Away (instant)");
        actionList.put(40, "Arm-Stay (instant)");
        actionList.put(41, "Disarm (instant)");
        actionList.put(42, "Arm-Away (Latch-Key)");
        actionList.put(43, "Arm-Away (Latch-Key instant)");
        actionList.put(44, "Set House-ID");
        actionList.put(45, "Enable timing");
        actionList.put(46, "Disable timing");
        actionList.put(47, "Set Time-Frames");
        actionList.put(48, "Enable temperature control");
        actionList.put(49, "Disable temperature control");
        actionList.put(50, "Set temperature parameters");
        actionList.put(51, "Enable over-temperature alarm");
        actionList.put(52, "Home Security");
        actionList.put(53, "Set over-temperature limit");
        actionList.put(54, "Enable rapid-temp. change alarm");
        actionList.put(55, "Disable rapid-temp. change alarm");
        actionList.put(56, "Set temp. increase within timeframe");
        actionList.put(57, "Enable socket-output SMS-notification");
        actionList.put(58, "Disable socket-output SMS-notification");
        actionList.put(59, "Enable power supply SMS-notification");
        actionList.put(60, "Disable power supply SMS-notification");
        actionList.put(61, "Enable warning tone");
        actionList.put(62, "Disable warning tone");

        Iterator it = actionList.entrySet().iterator();
        ContentValues cv;

        while(it.hasNext()) {
            Map.Entry e = (Map.Entry)it.next();

            cv = new ContentValues();
            cv.put(ActionEntry._ID, (Integer) e.getKey());
            cv.put(ActionEntry.COLUMN_NAME_NAME, (String) e.getValue());
            cv.put(ActionEntry.COLUMN_NAME_ICON, R.drawable.ic_refresh_black_24dp);
            db.insert(ActionEntry.TABLE_NAME, null, cv);
        }
    }

    private void insertDevices(SQLiteDatabase db) {
        //final Map<Integer, Array<String, Integer, String> deviceList = new HashMap<Integer, String>();
        final List<ContentValues> deviceList = new LinkedList<ContentValues>();

        ContentValues cv;

        cv = new ContentValues(); cv.put(DeviceEntry._ID, 1); cv.put(DeviceEntry.COLUMN_NAME_NAME, "LHD-8003"); cv.put(DeviceEntry.COLUMN_NAME_TYPE_ID, "1"); cv.put(DeviceEntry.COLUMN_NAME_PRODUCER, "LongHorn-CSST"); deviceList.add(cv);
        cv = new ContentValues(); cv.put(DeviceEntry._ID, 2); cv.put(DeviceEntry.COLUMN_NAME_NAME, "DRH-301 (Master)"); cv.put(DeviceEntry.COLUMN_NAME_TYPE_ID, "2"); cv.put(DeviceEntry.COLUMN_NAME_PRODUCER, "GSM-One"); deviceList.add(cv);
        cv = new ContentValues(); cv.put(DeviceEntry._ID, 3); cv.put(DeviceEntry.COLUMN_NAME_NAME, "DRH-301 (Family)"); cv.put(DeviceEntry.COLUMN_NAME_TYPE_ID, "2"); cv.put(DeviceEntry.COLUMN_NAME_PRODUCER, "GSM-One"); deviceList.add(cv);
        cv = new ContentValues(); cv.put(DeviceEntry._ID, 4); cv.put(DeviceEntry.COLUMN_NAME_NAME, "AP801G"); cv.put(DeviceEntry.COLUMN_NAME_TYPE_ID, "2"); cv.put(DeviceEntry.COLUMN_NAME_PRODUCER, "OR"); deviceList.add(cv);
        cv = new ContentValues(); cv.put(DeviceEntry._ID, 5); cv.put(DeviceEntry.COLUMN_NAME_NAME, "AMGOVIS GSM+"); cv.put(DeviceEntry.COLUMN_NAME_TYPE_ID, "1"); cv.put(DeviceEntry.COLUMN_NAME_PRODUCER, "AMG"); deviceList.add(cv);
        cv = new ContentValues(); cv.put(DeviceEntry._ID, 6); cv.put(DeviceEntry.COLUMN_NAME_NAME, "ST-IVB"); cv.put(DeviceEntry.COLUMN_NAME_TYPE_ID, "1"); cv.put(DeviceEntry.COLUMN_NAME_PRODUCER, "iAlarm"); deviceList.add(cv);
//   cv = new ContentValues(); cv.put(DeviceEntry._ID, 7); cv.put(DeviceEntry.COLUMN_NAME_NAME, "FC-400"); cv.put(DeviceEntry.COLUMN_NAME_TYPE_ID, "1"); cv.put(DeviceEntry.COLUMN_NAME_PRODUCER, "iAlarm"); deviceList.add(cv);
//   cv = new ContentValues(); cv.put(DeviceEntry._ID, 8); cv.put(DeviceEntry.COLUMN_NAME_NAME, "GLS STIII-B"); cv.put(DeviceEntry.COLUMN_NAME_TYPE_ID, "1"); cv.put(DeviceEntry.COLUMN_NAME_PRODUCER, "FR"); deviceList.add(cv);
//   cv = new ContentValues(); cv.put(DeviceEntry._ID, 9); cv.put(DeviceEntry.COLUMN_NAME_NAME, "ST-VB"); cv.put(DeviceEntry.COLUMN_NAME_TYPE_ID, "1"); cv.put(DeviceEntry.COLUMN_NAME_PRODUCER, "iAlarm"); deviceList.add(cv);
//   cv = new ContentValues(); cv.put(DeviceEntry._ID, 10); cv.put(DeviceEntry.COLUMN_NAME_NAME, "Switchbox-GSM"); cv.put(DeviceEntry.COLUMN_NAME_TYPE_ID, "2"); cv.put(DeviceEntry.COLUMN_NAME_PRODUCER, "antrax"); deviceList.add(cv);
        cv = new ContentValues(); cv.put(DeviceEntry._ID, 11); cv.put(DeviceEntry.COLUMN_NAME_NAME, "Touch-Less"); cv.put(DeviceEntry.COLUMN_NAME_TYPE_ID, "3"); cv.put(DeviceEntry.COLUMN_NAME_PRODUCER, "Rika"); deviceList.add(cv);
//   cv = new ContentValues(); cv.put(DeviceEntry._ID, 12); cv.put(DeviceEntry.COLUMN_NAME_NAME, "Touch"); cv.put(DeviceEntry.COLUMN_NAME_TYPE_ID, "3"); cv.put(DeviceEntry.COLUMN_NAME_PRODUCER, "Rika"); deviceList.add(cv);
        cv = new ContentValues(); cv.put(DeviceEntry._ID, 13); cv.put(DeviceEntry.COLUMN_NAME_NAME, "B2 (Master)"); cv.put(DeviceEntry.COLUMN_NAME_TYPE_ID, "5"); cv.put(DeviceEntry.COLUMN_NAME_PRODUCER, "Tuta"); deviceList.add(cv);
        cv = new ContentValues(); cv.put(DeviceEntry._ID, 14); cv.put(DeviceEntry.COLUMN_NAME_NAME, "B2 (Family)"); cv.put(DeviceEntry.COLUMN_NAME_TYPE_ID, "5"); cv.put(DeviceEntry.COLUMN_NAME_PRODUCER, "Tuta"); deviceList.add(cv);
        cv = new ContentValues(); cv.put(DeviceEntry._ID, 15); cv.put(DeviceEntry.COLUMN_NAME_NAME, "G5"); cv.put(DeviceEntry.COLUMN_NAME_TYPE_ID, "1"); cv.put(DeviceEntry.COLUMN_NAME_PRODUCER, "Chuango"); deviceList.add(cv);
//   cv = new ContentValues(); cv.put(DeviceEntry._ID, ); cv.put(DeviceEntry.COLUMN_NAME_NAME, "GS-M2C"); cv.put(DeviceEntry.COLUMN_NAME_TYPE_ID, "1"); cv.put(DeviceEntry.COLUMN_NAME_PRODUCER, "GlobalSafe"); deviceList.add(cv);
//   cv = new ContentValues(); cv.put(DeviceEntry._ID, ); cv.put(DeviceEntry.COLUMN_NAME_NAME, "GS-M2BX"); cv.put(DeviceEntry.COLUMN_NAME_TYPE_ID, "1"); cv.put(DeviceEntry.COLUMN_NAME_PRODUCER, "GlobalSafe"); deviceList.add(cv);
//   cv = new ContentValues(); cv.put(DeviceEntry._ID, ); cv.put(DeviceEntry.COLUMN_NAME_NAME, "707"); cv.put(DeviceEntry.COLUMN_NAME_TYPE_ID, "2"); cv.put(DeviceEntry.COLUMN_NAME_PRODUCER, "iSocket"); deviceList.add(cv);
//   cv = new ContentValues(); cv.put(DeviceEntry._ID, ); cv.put(DeviceEntry.COLUMN_NAME_NAME, "706 Light"); cv.put(DeviceEntry.COLUMN_NAME_TYPE_ID, "2"); cv.put(DeviceEntry.COLUMN_NAME_PRODUCER, "iSocket"); deviceList.add(cv);
//   cv = new ContentValues(); cv.put(DeviceEntry._ID, ); cv.put(DeviceEntry.COLUMN_NAME_NAME, "ENER022"); cv.put(DeviceEntry.COLUMN_NAME_TYPE_ID, "2"); cv.put(DeviceEntry.COLUMN_NAME_PRODUCER, "Energenie"); deviceList.add(cv);
//   cv = new ContentValues(); cv.put(DeviceEntry._ID, ); cv.put(DeviceEntry.COLUMN_NAME_NAME, "AlarmBox-GSM"); cv.put(DeviceEntry.COLUMN_NAME_TYPE_ID, "2"); cv.put(DeviceEntry.COLUMN_NAME_PRODUCER, "antrax"); deviceList.add(cv);
        cv = new ContentValues(); cv.put(DeviceEntry._ID, 18); cv.put(DeviceEntry.COLUMN_NAME_NAME, "S-30 (V2) (Master)"); cv.put(DeviceEntry.COLUMN_NAME_TYPE_ID, "2"); cv.put(DeviceEntry.COLUMN_NAME_PRODUCER, "Tuta"); deviceList.add(cv);
//   cv = new ContentValues(); cv.put(DeviceEntry._ID, ); cv.put(DeviceEntry.COLUMN_NAME_NAME, "Touch-Less + Sensor"); cv.put(DeviceEntry.COLUMN_NAME_TYPE_ID, "3"); cv.put(DeviceEntry.COLUMN_NAME_PRODUCER, "Rika"); deviceList.add(cv);
//   cv = new ContentValues(); cv.put(DeviceEntry._ID, ); cv.put(DeviceEntry.COLUMN_NAME_NAME, "B2 (Guest)"); cv.put(DeviceEntry.COLUMN_NAME_TYPE_ID, "5"); cv.put(DeviceEntry.COLUMN_NAME_PRODUCER, "Tuta"); deviceList.add(cv);
//   cv = new ContentValues(); cv.put(DeviceEntry._ID, ); cv.put(DeviceEntry.COLUMN_NAME_NAME, "ME Control"); cv.put(DeviceEntry.COLUMN_NAME_TYPE_ID, "3"); cv.put(DeviceEntry.COLUMN_NAME_PRODUCER, "Mitsubishi"); deviceList.add(cv);
        cv = new ContentValues(); cv.put(DeviceEntry._ID, 16); cv.put(DeviceEntry.COLUMN_NAME_NAME, "XMD-3200.pro"); cv.put(DeviceEntry.COLUMN_NAME_TYPE_ID, "1"); cv.put(DeviceEntry.COLUMN_NAME_PRODUCER, "VisorTech"); deviceList.add(cv);
//   cv = new ContentValues(); cv.put(DeviceEntry._ID, ); cv.put(DeviceEntry.COLUMN_NAME_NAME, "GSM Alarm System"); cv.put(DeviceEntry.COLUMN_NAME_TYPE_ID, "1"); cv.put(DeviceEntry.COLUMN_NAME_PRODUCER, "Euro1Supply"); deviceList.add(cv);
        cv = new ContentValues(); cv.put(DeviceEntry._ID, 17); cv.put(DeviceEntry.COLUMN_NAME_NAME, "Powermaster-10"); cv.put(DeviceEntry.COLUMN_NAME_TYPE_ID, "1"); cv.put(DeviceEntry.COLUMN_NAME_PRODUCER, "Visonic"); deviceList.add(cv);
        cv = new ContentValues(); cv.put(DeviceEntry._ID, 19); cv.put(DeviceEntry.COLUMN_NAME_NAME, "S-30 (V2) (Family)"); cv.put(DeviceEntry.COLUMN_NAME_TYPE_ID, "2"); cv.put(DeviceEntry.COLUMN_NAME_PRODUCER, "Tuta"); deviceList.add(cv);


        for (ContentValues cvE: deviceList) {
            db.insert(DeviceEntry.TABLE_NAME, null, cvE);
        }

    }

    private void insertDevicesActions(SQLiteDatabase sqLiteDatabase) {
        //TODO: blow up with parameters
        //further table DeviceActionParameters
        // deviceId, ActionID, ParameterId, ParameterType, ParameterTargetUnit, ParameterMax, ParameterMin, ParameterStepwidth
        // nextTable parameterTargetUnits, parameterType: temperature -> degrees, fahrenheit, ...

        ContentValues cv = new ContentValues();
        /*cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 1);
        cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 2);
        cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "Password__Armaway");
        sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME, null, cv);

        cv = new ContentValues();
        cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 1);
        cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 3);
        cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "Password__Armastay");
        sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME, null, cv);

        cv = new ContentValues();
        cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 1);
        cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 4);
        cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "Password__Disarm");
        sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME, null, cv);


        cv = new ContentValues();
        cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 1);
        cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 3);
        cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "Password__Status");
        sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME, null, cv);*/

        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 1); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 2);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "Password__Armaway");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 1); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 3);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "Password__Armstay");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 1); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 4);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "Password__Disarm");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 1); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 1);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "Password__Status");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 4); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 7);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "SN__CHANGE");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 4); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 16);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "SN__CHECK");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 4); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 5);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "SN__ON");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 4); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 6);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "SN__OFF");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 5); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 1);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "__check");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 5); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 2);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "__arm");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 5); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 3);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "__parm");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 5); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 4);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "__disarm");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 10); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 5);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "On!__");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 10); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 6);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "Off!__");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 10); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 16);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "Status?__");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 10); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 7);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "Reset!__");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 11); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 17);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "__ ON");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 11); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 18);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "__ OFF");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 13); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 19);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#07#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 13); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 14);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#03#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 13); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 9);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#01#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 13); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 20);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#02#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 13); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 10);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#128#1#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 13); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 11);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#128#0#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 13); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 12);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#118#1#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 13); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 13);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#118#0#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 13); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 15);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#03#1#1#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 14); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 19);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#07#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 14); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 14);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#03#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 14); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 9);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#01#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 14); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 20);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#02#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 14); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 10);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#128#1#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 14); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 11);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#128#0#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 14); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 12);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#118#1#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 14); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 13);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#118#0#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 15); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 2);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "1");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 15); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 3);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "2");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 15); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 4);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "0");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 15); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 31);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "3");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 15); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 24);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "11");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 15); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 32);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "12");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 15); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 1);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "00");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 15); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 27);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "?");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 15); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 37);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "??");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 15); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 38);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "???");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 15); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 33);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "7");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 15); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 34);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "8");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 15); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 35);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "10");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 15); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 36);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "13");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 6); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 1);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "password:__\n system status");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 6); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 2);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "password:__\n system arm");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 6); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 3);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "password:__\n system home");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 6); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 4);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "password:__\n system disarm");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 6); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 21);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "password:__\n alarm cancel");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 16); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 2);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "1");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 16); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 4);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "0");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 16); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 22);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "2");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 16); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 23);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "3");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 16); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 24);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "5");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 16); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 26);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "62");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 16); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 25);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "61");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 16); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 27);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "?");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 16); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 30);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "0001");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 16); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 28);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "0049");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 16); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 29);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "0033");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 17); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 2);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "AW __");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 17); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 4);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "DA __");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 17); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 3);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "HM __");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 17); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 1);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "ST __");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 17); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 42);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "LK __");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 17); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 43);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "LKI __");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 17); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 39);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "AWI __");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 17); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 40);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "HMI __");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 17); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 44);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "HN __ {replace with new ID}");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 18); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 16);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#07#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 18); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 5);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#01#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 18); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 6);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#02#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 18); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 45);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#128#1#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 18); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 46);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#128#0#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 18); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 47);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#129#WorkDay#StartTime#EndTime#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 18); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 48);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#159#1#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 18); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 49);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#159#0#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 18); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 50);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#159#Mode#LowTemp#HighTemp#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 18); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 51);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#170#1#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 18); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 52);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#170#0#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 18); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 53);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#170#MinTemp#MaxTemp#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 18); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 54);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#160#1#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 18); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 55);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#160#0#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 18); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 56);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#160#Temp#Time#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 18); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 57);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#11#1#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 18); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 58);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#11#0#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 18); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 59);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#12#1#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 18); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 60);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#12#0#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 18); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 61);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#19#1#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 18); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 62);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#19#0#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 2); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 16);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#07#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 2); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 5);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#01#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 2); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 6);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#02#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 2); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 45);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#128#1#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 2); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 46);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#128#0#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 2); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 47);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#129#WorkDay#StartTime#EndTime#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 2); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 48);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#159#1#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 2); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 49);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#159#0#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 2); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 50);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#159#Mode#LowTemp#HighTemp#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 2); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 51);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#170#1#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 2); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 52);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#170#0#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 2); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 53);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#170#MinTemp#MaxTemp#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 2); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 54);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#160#1#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 2); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 55);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#160#0#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 2); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 56);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#160#Temp#Time#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 2); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 57);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#11#1#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 2); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 58);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#11#0#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 2); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 59);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#12#1#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 2); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 60);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#12#0#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 2); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 61);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#19#1#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 2); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 62);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#19#0#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 3); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 16);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#000#__#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 3); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 5);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#01#__#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 3); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 6);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#02#__#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 19); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 16);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#000#__#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 19); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 5);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#01#__#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);
        cv = new ContentValues(); cv.put(DeviceActionEntry.COLUMN_NAME_DEVICE_ID, 19); cv.put(DeviceActionEntry.COLUMN_NAME_ACTION_ID, 6);         cv.put(DeviceActionEntry.COLUMN_NAME_COMMAND, "#02#__#");   sqLiteDatabase.insert(DeviceActionEntry.TABLE_NAME,null,cv);

        /*query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (1, 2, 'Password__Armaway')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (1, 3, 'Password__Armstay')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (1, 4, 'Password__Disarm')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (1, 1, 'Password__Status')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (4, 7, 'SN__CHANGE')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (4, 16, 'SN__CHECK')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (4, 5, 'SN__ON')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (4, 6, 'SN__OFF')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (5, 1, '__check')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (5, 2, '__arm')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (5, 3, '__parm')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (5, 4, '__disarm')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (10, 5, 'On!__')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (10, 6, 'Off!__')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (10, 16, 'Status?__')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (10, 7, 'Reset!__')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (11, 17, '__ ON')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (11, 18, '__ OFF')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (13, 19, '#07#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (13, 14, '#03#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (13, 9, '#01#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (13, 20, '#02#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (13, 10, '#128#1#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (13, 11, '#128#0#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (13, 12, '#118#1#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (13, 13, '#118#0#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (13, 15, '#03#1#1#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (14, 19, '#07#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (14, 14, '#03#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (14, 9, '#01#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (14, 20, '#02#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (14, 10, '#128#1#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (14, 11, '#128#0#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (14, 12, '#118#1#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (14, 13, '#118#0#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (15, 2, '1')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (15, 3, '2')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (15, 4, '0')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (15, 31, '3')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (15, 24, '11')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (15, 32, '12')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (15, 1, '00')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (15, 27, '?')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (15, 37, '??')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (15, 38, '???')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (15, 33, '7')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (15, 34, '8')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (15, 35, '10')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (15, 36, '13')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (6, 1, 'password:__\n system status')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (6, 2, 'password:__\n system arm')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (6, 3, 'password:__\n system home')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (6, 4, 'password:__\n system disarm')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (6, 21, 'password:__\n alarm cancel')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (16, 2, '1')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (16, 4, '0')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (16, 22, '2')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (16, 23, '3')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (16, 24, '5')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (16, 26, '62')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (16, 25, '61')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (16, 27, '?')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (16, 30, '0001')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (16, 28, '0049')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (16, 29, '0033')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (17, 2, 'AW __')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (17, 4, 'DA __')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (17, 3, 'HM __')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (17, 1, 'ST __')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (17, 42, 'LK __')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (17, 43, 'LKI __')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (17, 39, 'AWI __')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (17, 40, 'HMI __')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (17, 44, 'HN __ {replace with new ID}')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (18, 16, '#07#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (18, 5, '#01#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (18, 6, '#02#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (18, 45, '#128#1#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (18, 46, '#128#0#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (18, 47, '#129#WorkDay#StartTime#EndTime#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (18, 48, '#159#1#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (18, 49, '#159#0#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (18, 50, '#159#Mode#LowTemp#HighTemp#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (18, 51, '#170#1#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (18, 52, '#170#0#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (18, 53, '#170#MinTemp#MaxTemp#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (18, 54, '#160#1#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (18, 55, '#160#0#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (18, 56, '#160#Temp#Time#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (18, 57, '#11#1#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (18, 58, '#11#0#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (18, 59, '#12#1#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (18, 60, '#12#0#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (18, 61, '#19#1#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (18, 62, '#19#0#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (2, 16, '#07#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (2, 5, '#01#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (2, 6, '#02#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (2, 45, '#128#1#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (2, 46, '#128#0#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (2, 47, '#129#WorkDay#StartTime#EndTime#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (2, 48, '#159#1#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (2, 49, '#159#0#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (2, 50, '#159#Mode#LowTemp#HighTemp#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (2, 51, '#170#1#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (2, 52, '#170#0#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (2, 53, '#170#MinTemp#MaxTemp#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (2, 54, '#160#1#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (2, 55, '#160#0#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (2, 56, '#160#Temp#Time#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (2, 57, '#11#1#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (2, 58, '#11#0#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (2, 59, '#12#1#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (2, 60, '#12#0#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (2, 61, '#19#1#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (2, 62, '#19#0#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (3, 16, '#000#__#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (3, 5, '#01#__#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (3, 6, '#02#__#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (19, 16, '#000#__#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (19, 5, '#01#__#')");
        query.exec("INSERT INTO tab_devicesactions (da_deviceID, da_actionID, da_command) VALUES (19, 6, '#02#__#')");

        "CREATE UNIQUE INDEX PK ON tab_devicesactions ( "
               "da_id ASC "
        ")"
        */
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + DeviceEntry.TABLE_NAME );
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TypeEntry.TABLE_NAME );
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + DeviceActionEntry.TABLE_NAME );
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + ActionEntry.TABLE_NAME );
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + ConfiguredDeviceEntry.TABLE_NAME);

        sqLiteDatabase.execSQL("DROP VIEW IF EXISTS " + ActionsForConfiguredDeviceEntry.VIEW_NAME);
        sqLiteDatabase.execSQL("DROP VIEW IF EXISTS " + ConfiguredDeviceDeepEntry.VIEW_NAME);


        onCreate(sqLiteDatabase);
    }
}
