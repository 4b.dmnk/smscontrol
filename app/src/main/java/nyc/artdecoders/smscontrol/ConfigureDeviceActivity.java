package nyc.artdecoders.smscontrol;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

//import org.dmnk.smscontrol.R;

import nyc.artdecoders.smscontrol.data.SMSControlContract;
import nyc.artdecoders.smscontrol.databinding.ActivityConfigureDeviceBinding;

//import nyc.artdecoders.smscontrol.

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.lang.reflect.Method;

public class ConfigureDeviceActivity extends AppCompatActivity
    implements  LoaderManager.LoaderCallbacks<Cursor> {

    static final int REQUEST_IMAGE_GET_BACKGROUND = 1;
    static final int REQUEST_IMAGE_GET_ICON = 2;

//    TextInputEditText mPhonenumber;
//    TextInputEditText mDeviceName;
//    TextInputEditText mPassword;
    TextInputEditText mColor;

//    TextView mMake;
//    TextView mModel;
//    TextView mTypeId;

    /*int deviceId;

    private SQLiteDatabase mDb;*/

    private Uri mUri;

    private static final int ID_DEVICE_LOADER = 21;

    private long mDeviceId;
    private ImageButton mBackgroundImageChooseButton;
    private ImageView mBackgroundImage;
    private String mBackgroundPath;
    private ImageView mIconImage;
    private String mIconPath;

    private ImageButton mIconImageChooseButton;

    ActivityConfigureDeviceBinding mBinding;
    ConfiguredDeviceViewModel mConfiguredDeviceViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configure_device);


        //mBinding = ActivityConfigureDeviceBinding.inflate(inflater, container, false);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_configure_device);


       /* SMSControlDbHelper dbHelper = new SMSControlDbHelper(this);
        mDb = dbHelper.getWritableDatabase();*/
        //dbHelper.onUpgrade(mDb, 1,1);

        //Intent intentThatStartedThisActivity = getIntent();
        mUri = getIntent().getData();
        if (mUri == null) throw new NullPointerException("URI for ConfigureDeviceActivity cannot be null");

        /*if (intentThatStartedThisActivity != null) {
            if (intentThatStartedThisActivity.hasExtra(Intent.EXTRA_INDEX)) {
                deviceId = intentThatStartedThisActivity.getIntExtra(Intent.EXTRA_INDEX, 0);

            }
        }*/

//        mMake = findViewById(R.id.DeviceMakeTextView);
//        mModel = findViewById(R.id.DeviceModelTextView);
//        mTypeId = findViewById(R.id.DeviceTypeLabel);
//        mColor = findViewById(R.id.ColorEditText);

        /*Cursor cursor = getDeviceDetails(deviceId);
        cursor.moveToFirst();

        mMake.setText(cursor.getString(cursor.getColumnIndex(SMSControlContract.DeviceEntry.COLUMN_NAME_PRODUCER)));
        mModel.setText(cursor.getString(cursor.getColumnIndex(SMSControlContract.DeviceEntry.COLUMN_NAME_NAME)));
        mTypeId.setText(cursor.getString(cursor.getColumnIndex(SMSControlContract.DeviceEntry.COLUMN_NAME_TYPE_ID)));*/

//        mPhonenumber = findViewById(R.id.PhonenumberEditText);
//        mDeviceName = findViewById(R.id.DescriptionEditText);
//        mPassword = findViewById(R.id.PasswordEditText);

        mBackgroundImage = findViewById(R.id.ivBackground);
        mBackgroundImageChooseButton = findViewById(R.id.ibSelectBackground);

        mBackgroundImageChooseButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(intent, REQUEST_IMAGE_GET_BACKGROUND);
                }
            }
        });

        mIconImage = findViewById(R.id.ivIcon);
        mIconImageChooseButton = findViewById(R.id.ibSelectIcon);

        mIconImageChooseButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(intent, REQUEST_IMAGE_GET_ICON);
                }
            }
        });

        getSupportLoaderManager().initLoader(ID_DEVICE_LOADER, null, this);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        String fileFolder, filename, filePath;
        Bitmap bitmap;
        FileOutputStream fos;

        //String mPos =  m//mTabHost.getCurrentTabTag().toString();
        //Uri uri = PoetcomContract.MedienEntry.CONTENT_URI;
        ContentResolver cr = getContentResolver();
        ContentValues cv;

        InputStream stream = null;

        if (resultCode == RESULT_OK) {
            switch (requestCode) {

                case REQUEST_IMAGE_GET_BACKGROUND:
                    try {
                        stream = getContentResolver().openInputStream(
                                data.getData());
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }

                    bitmap = BitmapFactory.decodeStream(stream);

                    fileFolder = getFilesDir() + "/";
                    filename = Long.toString(System.currentTimeMillis() / 1000) + ".jpg";
                    filePath = fileFolder + filename;

                    try {
                        fos = openFileOutput(filename, Context.MODE_PRIVATE);
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, fos);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }

                    mBackgroundImage.setImageURI(Uri.parse(filePath));
                    mBackgroundPath = filePath;
                    break;

                case REQUEST_IMAGE_GET_ICON:
                    try {
                        stream = getContentResolver().openInputStream(
                                data.getData());
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }

                    bitmap = BitmapFactory.decodeStream(stream);

                    fileFolder = getFilesDir() + "/";
                    filename = Long.toString(System.currentTimeMillis() / 1000) + ".jpg";
                    filePath = fileFolder + filename;

                    try {
                        fos = openFileOutput(filename, Context.MODE_PRIVATE);
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, fos);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }

                    mIconImage.setImageURI(Uri.parse(filePath));
                    mIconPath = filePath;
                    break;
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.save, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int itemId = item.getItemId();

        switch (itemId) {
            case R.id.action_save:
                if(mConfiguredDeviceViewModel.hasErrors()) {
                    Snackbar.make(mBinding.getRoot(),
                            R.string.mandatory_fields_error, Snackbar.LENGTH_SHORT).show();

                    return true;
                }
                ContentValues cv = mConfiguredDeviceViewModel.getPendingChanges();
                cv.put(SMSControlContract.ConfiguredDeviceEntry.COLUMN_NAME_DEVICE_ID, mConfiguredDeviceViewModel.getDeviceId());

//                ContentValues cv = new ContentValues();
//                cv.put(SMSControlContract.ConfiguredDeviceEntry.COLUMN_NAME_DEVICE_ID, mDeviceId);
//                cv.put(SMSControlContract.ConfiguredDeviceEntry.COLUMN_NAME_DESCRIPTION, mDeviceName.getText().toString());
//                cv.put(SMSControlContract.ConfiguredDeviceEntry.COLUMN_NAME_PASSWORD, mPassword.getText().toString());
//                cv.put(SMSControlContract.ConfiguredDeviceEntry.COLUMN_NAME_PHONENUMBER, mPhonenumber.getText().toString());

                if(mBackgroundPath != null && mBackgroundPath.length() > 0) {
                    cv.put(SMSControlContract.ConfiguredDeviceEntry.COLUMN_NAME_CUSTOM_BACKGROUND, mBackgroundPath);
                }
                if(mIconPath != null && mIconPath.length() > 0) {
                    cv.put(SMSControlContract.ConfiguredDeviceEntry.COLUMN_NAME_CUSTOM_IMAGE, mIconPath);
                }
                if(mColor != null && mColor.getText().length() > 0) {
                    cv.put(SMSControlContract.ConfiguredDeviceEntry.COLUMN_NAME_COLOR, mColor.getText().toString());
                }

                Uri insertUri = SMSControlContract.ConfiguredDeviceEntry.CONTENT_URI;
                ContentResolver r = getContentResolver();

                Uri insertedDevice = r.insert(insertUri, cv);

                if(insertedDevice!= null) {
                    setResult(RESULT_OK);
                    finish();
                }
                /*mAdapter = new GreenAdapter(NUM_LIST_ITEMS);
                mNumbersList.setAdapter(mAdapter);*/
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /*private Cursor getDeviceDetails(int DeviceId) {
        return mDb.query(
                SMSControlContract.DeviceEntry.TABLE_NAME,
                null,
                SMSControlContract.DeviceEntry._ID  +  "= "+DeviceId,
                null,
                null,
                null,
                SMSControlContract.DeviceEntry.COLUMN_NAME_TYPE_ID
        );
    }*/

    @Override
    public Loader<Cursor> onCreateLoader(int loaderId, Bundle loaderArgs) {

        switch (loaderId) {
            case ID_DEVICE_LOADER:

                return new CursorLoader(this,
                        mUri,
                        null,
                        null,
                        null,
                        null);

            default:
                throw new RuntimeException("Loader Not Implemented: " + loaderId);
        }
    }
    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        switch (loader.getId()) {
            case ID_DEVICE_LOADER:

                boolean cursorHasValidData = false;
                if (data != null && data.moveToFirst()) {
                    /* We have valid data, continue on to bind the data to the UI */
                    cursorHasValidData = true;
                }

                if (!cursorHasValidData) {
                    /* No data to display, simply return and do nothing */
                    return;
                }

                int producerIndex = data.getColumnIndex(SMSControlContract.DeviceEntry.COLUMN_NAME_PRODUCER);
                int nameIndex = data.getColumnIndex(SMSControlContract.DeviceEntry.COLUMN_NAME_NAME);
                int typeIdIndex = data.getColumnIndex(SMSControlContract.DeviceEntry.COLUMN_NAME_TYPE_ID);
                int deviceIdIndex = data.getColumnIndex(SMSControlContract.DeviceEntry._ID);

                //mDeviceId = data.getLong(idIndex);

                //mMake.setText(data.getString(producerIndex));
                //mModel.setText(data.getString(nameIndex));
                //mTypeId.setText(data.getString(typeIndex));

                //for now always create
                mConfiguredDeviceViewModel = new ConfiguredDeviceViewModel(-1L,
                        data.getLong(deviceIdIndex),
                        data.getLong(typeIdIndex),
                        data.getString(nameIndex),
                        data.getString(producerIndex),
                        "",
                        "",
                        "");

                mBinding.setDevice(mConfiguredDeviceViewModel);


                bindValidator(mBinding.getRoot().findViewById(R.id.DescriptionEditText), "validateDescription", mConfiguredDeviceViewModel);
                bindValidator(mBinding.getRoot().findViewById(R.id.PhonenumberEditText), "validatePhonenumber", mConfiguredDeviceViewModel);
                bindValidator(mBinding.getRoot().findViewById(R.id.PasswordEditText), "validatePassword", mConfiguredDeviceViewModel);

                break;

            default:
                throw new RuntimeException("loader not implemented!");
        }


    }

    public void bindValidator(View v, String reflectionMethod, final ConfiguredDeviceViewModel avm) {
        try {
            final Method m = avm.getClass().getMethod(reflectionMethod, null);

            v.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if(!hasFocus) { //so it had it before
                        try {
                            m.invoke(avm, null);

                            //if((boolean) retVal) { // value inverted (was ok / isn't)
                            /*ContentValues cv = avm.getPendingChanges();
                            if (cv.size() > 0) {
                                getContentResolver().update(mUri, cv, null, null);
                            }*/
                        } catch (Exception e) {

                        }
                    }
                }
            });

            ((TextInputEditText) v).addTextChangedListener(new TextWatcher() {

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {  }

                @Override
                public void afterTextChanged(Editable s) {
                    if(avm!= null) {
                        try {
                            m.invoke(avm, null);
                        } catch (Exception e) {

                        }
                    }
                }
            });
        } catch (Exception e) {
            Log.d("anonewfrag:", e.toString());
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }
}
