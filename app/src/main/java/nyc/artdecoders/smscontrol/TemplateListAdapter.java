package nyc.artdecoders.smscontrol;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import nyc.artdecoders.smscontrol.data.SMSControlContract;

import nyc.artdecoders.smscontrol.databinding.ListItemTemplateBinding;

/**
 * Created by Dominik on 01.02.2018.
 */

public class TemplateListAdapter extends RecyclerView.Adapter<TemplateListAdapter.TemplateViewHolder> {

    private Context mContext;
    // COMPLETED (8) Add a new local variable mCount to store the count of items to be displayed in the recycler view
    private Cursor mCursor;

    private ListItemTemplateBinding mLiab;

    public interface ListItemClickListener {
        void onListItemClick(int clickedItemIndex);
    }

    final private ListItemClickListener mOnClickListener;

    // COMPLETED (9) Update the Adapter constructor to accept an integer for the count along with the context
    /**
     * Constructor using the context and the db cursor
     *
     * @param context the calling context/activity
     */
    public TemplateListAdapter(Context context, ListItemClickListener cl) {
        this.mContext = context;
        // COMPLETED (10) Set the local mCount to be equal to count
        mOnClickListener = cl;
        //mCursor = cursor;
    }

    @Override
    public TemplateViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // Get the RecyclerView item layout
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.list_item_template, parent, false);

        //mLiab = ListItemTemplateBinding.inflate(inflater, parent, false);
        return new TemplateViewHolder(view);
        //return new TemplateViewHolder(mLiab.getRoot());
    }

    @Override
    public void onBindViewHolder(TemplateViewHolder holder, int position) {

        if (!mCursor.moveToPosition(position))
            return; // bail if returned null

        int id = mCursor.getInt(mCursor.getColumnIndex(SMSControlContract.DeviceEntry._ID));

        String name = mCursor.getString(mCursor.getColumnIndex(SMSControlContract.DeviceEntry.COLUMN_NAME_NAME));
        String vendor = mCursor.getString(mCursor.getColumnIndex(SMSControlContract.DeviceEntry.COLUMN_NAME_PRODUCER));
        String type = mCursor.getString(mCursor.getColumnIndex(SMSControlContract.DeviceEntry.COLUMN_NAME_TYPE_ID));

        //ListItemDevice lid = new ListItemDevice(id, name, vendor, type);
        //mLiab.setDevice(lid);
        //mLiab.executePendingBindings();

        //((TextView) mLiab.getRoot().findViewById(R.id.device_text_view)).setText(name);
        holder.deviceNameTextView.setText(name);
        holder.vendorTextView.setText(vendor);
        holder.ivType.setImageResource(getTypeImage(type));

        holder.itemView.setTag(id);
    }

    private int getTypeImage(String type) {
        int iconResource;

        //String type = Long.toString(typeId);
        if (type == null) {
            type = "0";
        }
        switch (type) {
            case "1":
                iconResource = R.drawable.ic_security_black_24dp;
                break;
            case "2":
                iconResource = R.drawable.ic_power_black_24dp;
                break;
            case "3":
                iconResource = R.drawable.ic_ac_unit_black_24dp;
                break;
            case "4":
                iconResource = R.drawable.ic_gps_fixed_black_24dp;
                break;
            case "5":
                iconResource = R.drawable.ic_videocam_black_24dp;
                break;
            default:
                iconResource = R.drawable.ic_not_interested_black_24dp;
                break;
        }

        return iconResource;
    }


    // COMPLETED (11) Modify the getItemCount to return the mCount value rather than 0
    @Override
    public int getItemCount() {

        if (mCursor == null) {
            return 0;
        }
        return mCursor.getCount();
    }

    public Cursor swapCursor(Cursor c) {
        // check if this cursor is the same as the previous cursor (mCursor)
        if (mCursor == c) {
            return null; // bc nothing has changed
        }
        Cursor temp = mCursor;
        mCursor = c; // new cursor value assigned

        //check if this is a valid cursor, then update the cursor
        if (c != null) {
            this.notifyDataSetChanged();
        }
        return temp;
    }

    /**
     * Inner class to hold the views needed to display a single item in the recycler-view
     */
    class TemplateViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener {

        // Will display the guest name
        TextView deviceNameTextView;
        // Will display the party size number
        TextView vendorTextView;
        AppCompatImageView ivType;
        //ListItemDevice mListItemDevice;


        /**
         * Constructor for our ViewHolder. Within this constructor, we get a reference to our
         * TextViews
         *
         * @param itemView The View that you inflated in
         *                 {@link TemplateListAdapter#onCreateViewHolder(ViewGroup, int)}
         */
        public TemplateViewHolder(View itemView) {
            super(itemView);
            deviceNameTextView = itemView.findViewById(R.id.device_text_view);
            vendorTextView = itemView.findViewById(R.id.vendor_text_view);
            ivType = itemView.findViewById(R.id.ivType);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int id = (int) v.getTag();
            mOnClickListener.onListItemClick(id);
        }


    }
}