package nyc.artdecoders.smscontrol;

import android.content.ContentValues;
import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.android.databinding.library.baseAdapters.BR;

import nyc.artdecoders.smscontrol.data.SMSControlContract;


public class ConfiguredDeviceViewModel extends BaseObservable {
    private long mId;
    private long mTypeId;
    private long mDeviceId;

    private String mModel;
    private String mMake;

    private String mDescription;
    private String mPhonenumber;
    private String mPassword;

    private boolean mDescriptionError, mPhonenumberError, mPasswordError;

    private ContentValues mPendingChanges = new ContentValues();

    public ConfiguredDeviceViewModel (long id, long deviceId, long typeId, String model, String make, String description, String phonenumber, String password) {
        mId = id;
        mDeviceId = deviceId;
        mTypeId = typeId;
        mModel = model;
        mMake = make;

        mDescription = description;
        mPhonenumber = phonenumber;
        mPassword = password;
    }

    @Bindable
    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        if(mDescription != null && mDescription.equals(description)) return;

        mDescription = description;
        notifyPropertyChanged(BR.description);

        mPendingChanges.put(SMSControlContract.ConfiguredDeviceEntry.COLUMN_NAME_DESCRIPTION, description);
    }

    @Bindable
    public String getPhonenumber() {
        return mPhonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        if (mPhonenumber != null && mPhonenumber.equals(phonenumber)) return;

        mPhonenumber = phonenumber;
        notifyPropertyChanged(BR.phonenumber);

        mPendingChanges.put(SMSControlContract.ConfiguredDeviceEntry.COLUMN_NAME_PHONENUMBER, phonenumber);
    }

    @Bindable
    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        if (mPassword != null && mPassword.equals(password)) return;

        mPassword = password;
        notifyPropertyChanged(BR.password);

        mPendingChanges.put(SMSControlContract.ConfiguredDeviceEntry.COLUMN_NAME_PASSWORD, password);
    }

    public String getModel() {
        return mModel;
    }

    public String getMake(){
        return mMake;
    }

    public long getDeviceId() {
        return mDeviceId;
    }

    public ContentValues getPendingChanges() {
        ContentValues returnChanges = mPendingChanges;
        mPendingChanges = new ContentValues(); //reset to 0

        return returnChanges;
    }

    public boolean validateDescription() {
        boolean errorChanged = false;

        if(mDescription == null || mDescription.length() == 0) {
            if(mDescriptionError != true) {
                errorChanged = true;
            }
            mDescriptionError = true;
        } else {
            if(mDescriptionError != false) {
                errorChanged = true;
            }
            mDescriptionError = false;
        }
        if(errorChanged) {
            notifyPropertyChanged(BR.descriptionError);

  //          mPendingChanges.put(SMSControlContract.ConfiguredDeviceEntry.COLUMN_NAME_DESCRIPTION, mDescriptionError ? "X" : "");
        }

        return mDescriptionError;
    }

    public boolean validatePhonenumber() {
        boolean errorChanged = false;

        if(mPhonenumber == null || mPhonenumber.length() == 0) {
            if(mPhonenumberError != true) {
                errorChanged = true;
            }
            mPhonenumberError = true;
        } else {
            if(mPhonenumberError != false) {
                errorChanged = true;
            }
            mPhonenumberError = false;
        }
        if(errorChanged) {
            notifyPropertyChanged(BR.phonenumberError);

 //           mPendingChanges.put(SMSControlContract.ConfiguredDeviceEntry.COLUMN_NAME_DESCRIPTION, mPhonenumberError ? "X" : "");
        }

        return mPhonenumberError;
    }

    public boolean validatePassword() {
        boolean errorChanged = false;

        if(mPassword == null || mPassword.length() == 0) {
            if(mPasswordError != true) {
                errorChanged = true;
            }
            mPasswordError = true;
        } else {
            if(mPasswordError != false) {
                errorChanged = true;
            }
            mPasswordError = false;
        }
        if(errorChanged) {
            notifyPropertyChanged(BR.passwordError);

//            mPendingChanges.put(SMSControlContract.ConfiguredDeviceEntry.COLUMN_NAME_DESCRIPTION, mPasswordError ? "X" : "");
        }

        return mPhonenumberError;
    }

    public boolean hasErrors() {
        validateDescription(); validatePhonenumber(); validatePassword();

        return (mDescriptionError || mPhonenumberError || mPasswordError);
    }

    @Bindable
    public boolean isDescriptionError() {
        return mDescriptionError;
    }

    @Bindable
    public boolean isPhonenumberError() {
        return mPhonenumberError;
    }

    @Bindable
    public boolean isPasswordError() {
        return mPasswordError;
    }

}
