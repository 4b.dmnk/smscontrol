package nyc.artdecoders.smscontrol;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import nyc.artdecoders.smscontrol.R;

import nyc.artdecoders.smscontrol.data.SMSControlContract;
import nyc.artdecoders.smscontrol.data.SMSControlDbHelper;

public class MainActivity extends AppCompatActivity implements ConfiguredListAdapter.ListItemClickListener,
        LoaderManager.LoaderCallbacks<Cursor>  {
    //before releasing first official version - public alpha
    //DONE (1.1): allow custom background
    //DONE (1.2): allow custom icon
    //DONE (1.2.1): show icon in configuredDevicesListView too, otherwise fallback to devicetype icon
    //TODO (1.3): allow custom color
    //DONE (1.4): swipe delete
    //TODO (1.4.1): custom list item background
    //todo (1.5) load in oncreate, bind in on create view

    //version 2 beta / release?
    //TODO (2.1): multiple parameters (temp, time, string, ...)
    //todo (2.2) allow device edit
    //todo (2.3)

    //version 3
    //TODO (3.1): support custom devices
    //TODO (3.2): some method to get the customized parameters (to incorporate them for all users) (with "share" checkbox)

    //version 4
    //TODO (4.1) command history
    //TODO (4.2) reply interpretation (or at least adding into history)

    //random enhancements
    //TODO (x.1): reorder devices
    //TODO (x.2): landscape
    //TODO (x.3): switch to bidirectional binding / mvvm / view binding
        // x.3.1: ConfigureDeviceActivity
        // x.3.2: DeviceActivity
    //TODO (x.4): group headers in lists (alarm control, power, ac, ...)
    //TODO (x.4): search for template selection

        private ConfiguredListAdapter mAdapter;

        private SQLiteDatabase mDb;
        private Toast mToast;
        private RecyclerView mRecyclerView;

        private static final int ID_CONFIGURED_DEVICE_LOADER = 20;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);

            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setLogo(R.drawable.applogo);

            getSupportActionBar().setDisplayUseLogoEnabled(true);

            mRecyclerView = findViewById(R.id.all_templates_list_view);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

            mRecyclerView.setHasFixedSize(true);

            /*SMSControlDbHelper dbHelper = new SMSControlDbHelper(this);
            mDb = dbHelper.getWritableDatabase();
            //dbHelper.onUpgrade(mDb, 1,1);
            Cursor cursor = getAllTemplates();*/

            mAdapter = new ConfiguredListAdapter(this, this);

            mRecyclerView.setAdapter(mAdapter);

            attachSwipeDeleteListener(mRecyclerView, this);

            getSupportLoaderManager().initLoader(ID_CONFIGURED_DEVICE_LOADER, null, this);

            FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.configure_device_btn);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                /*Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/
                    Intent i = new Intent(MainActivity.this, SelectTemplateActivity.class); //todo: why doesn't work this
                /*EditText editText = (EditText) findViewById(R.id.editText);
                String message = editText.getText().toString();
                intent.putExtra(EXTRA_MESSAGE, message);*/

                    // https://developer.android.com/training/basics/intents/result.html
                    startActivityForResult(i, 0);
                }
            });

            //TODO: two pane setup
            // https://github.com/udacity/Android_Me/blob/TFragments.07-Solution-TwoPaneUI/app/src/main/java/com/example/android/android_me/ui/MainActivity.java
        }

        private void attachSwipeDeleteListener(RecyclerView recyclerView, final Context mContext) {
            new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0,
                    ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
                @Override
                public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                    return false;
                }

                @Override
                public void onSwiped(final RecyclerView.ViewHolder viewHolder, int direction) {
                    AlertDialog.Builder adb = new AlertDialog.Builder(mContext);
                    adb.setMessage("Delete Device?").setTitle("Device deletion confirmation");

                    adb.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //user wants deletion

                            //long itemId = (long) viewHolder.itemView.getTag();

                            long itemId =  (long) viewHolder.itemView.getTag();


                            Uri uri = SMSControlContract.ConfiguredDeviceEntry.CONTENT_URI;
                            uri =  uri.buildUpon().appendPath(Long.toString(itemId)).build();
                            getContentResolver().delete(uri, null, null);

                            String textToShow = "device deleted";
                            Toast.makeText(mContext, textToShow, Toast.LENGTH_LONG).show();

                            getSupportLoaderManager().restartLoader(ID_CONFIGURED_DEVICE_LOADER, null, MainActivity.this);
                            return;
                        }
                    });
                    adb.setNegativeButton("nok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //user changed mind.
                            getSupportLoaderManager().restartLoader(ID_CONFIGURED_DEVICE_LOADER, null, MainActivity.this);
                            return;
                        }
                    });

                    AlertDialog ad = adb.create();
                    ad.show();

                /*long id = (long) viewHolder.itemView.getTag();

                Uri uri = PoetcomContract.MeldungEntry.CONTENT_URI;
                uri = uri.buildUpon().appendPath(Long.toString(id)).build();

                getContentResolver().delete(uri, null, null);
                getSupportLoaderManager().restartLoader(REPORT_LOADER_ID, null, MachineActivity.this);*/
                /*removeReport(id);
                mAdapter.swapCursor(getAllReports());*/
                }

                @Override
                public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                    //super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
                    View itemView = viewHolder.itemView;
                    int itemHeight = itemView.getHeight();// - itemView.top
                    ColorDrawable cd = new ColorDrawable();
                    int bgColor = Color.parseColor("#f44336");

                    Drawable deleteIcon = ContextCompat.getDrawable(mContext, R.drawable.ic_delete_black_24dp);
                    int intrinsicWidth = deleteIcon.getIntrinsicWidth();
                    int intrinsicHeight = deleteIcon.getIntrinsicHeight();
                    cd.setColor(bgColor);
                    cd.setBounds(itemView.getRight() + Math.round(dX), itemView.getTop(), itemView.getRight(), itemView.getBottom());
                    cd.draw(c);

                    int deleteIconTop = itemView.getTop() + (itemHeight - intrinsicHeight)/2;
                    int deleteIconMargin = (itemHeight - intrinsicHeight) /2;
                    int deleteIconLeft = itemView.getRight() - deleteIconMargin - intrinsicWidth;
                    int deleteIconRight = itemView.getRight() - deleteIconMargin;
                    int deleteIconBottom = deleteIconTop + intrinsicHeight;
                    deleteIcon.setBounds(deleteIconLeft, deleteIconTop, deleteIconRight, deleteIconBottom);

                    ImageView iv = new ImageView(mContext);
                    iv.setImageDrawable(deleteIcon);
                    iv.setScrollX(5050);
                    iv.draw(c);

                    super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
                }

            }).attachToRecyclerView(recyclerView);
        }

        private Cursor getAllTemplates() {
            // COMPLETED (6) Inside, call query on mDb passing in the table name and projection String [] order by COLUMN_TIMESTAMP
            return mDb.query(
                    SMSControlContract.DeviceEntry.TABLE_NAME,
                    null,
                    null,
                    null,
                    null,
                    null,
                    SMSControlContract.DeviceEntry.COLUMN_NAME_TYPE_ID
            );
        }

        @Override
        public void onListItemClick(long clickedItemIndex) {
            if(mToast != null) {
                mToast.cancel();
            }


        /*String toastMessage = "Item #" + clickedItemIndex + " clicked.";
        mToast = Toast.makeText(this, toastMessage, Toast.LENGTH_LONG);
        mToast.show();*/



            Intent i = new Intent(this, DeviceActivity.class);
            //i.putExtra(Intent.EXTRA_INDEX, clickedItemIndex);
            Uri uri = SMSControlContract.ConfiguredDeviceEntry.CONTENT_URI.buildUpon()
                    .appendPath(Long.toString(clickedItemIndex)).build();
            i.setData(uri);
            startActivity(i);
        }

        @Override
        public Loader<Cursor> onCreateLoader(int loaderId, Bundle args) {

            switch (loaderId) {
                case ID_CONFIGURED_DEVICE_LOADER:
                    Uri forecastQueryUri = SMSControlContract.ConfiguredDeviceDeepEntry.CONTENT_URI;
                    //String sortOrder = SMSControlContract.DeviceEntry.COLUMN_DATE + " ASC";
                    //String selection = SMSControlContract.WeatherEntry.getSqlSelectForTodayOnwards();

                    return new CursorLoader(this,
                            forecastQueryUri,
                            null,
                            null,
                            null,
                            null);

                default:
                    throw new RuntimeException("Loader Not Implemented: " + loaderId);
            }
        }

        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
            mAdapter.swapCursor(data);
            //if (mPosition == RecyclerView.NO_POSITION) mPosition = 0;
            //mRecyclerView.smoothScrollToPosition(mPosition);
            //if (data.getCount() != 0);// showWeatherDataView();
        }

        @Override
        public void onLoaderReset(Loader<Cursor> loader) {
            mAdapter.swapCursor(null);
        }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        /* Use AppCompatActivity's method getMenuInflater to get a handle on the menu inflater */
        MenuInflater inflater = getMenuInflater();
        /* Use the inflater's inflate method to inflate our menu layout to this menu */
        inflater.inflate(R.menu.main, menu);
        /* Return true so that the menu is displayed in the Toolbar */
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        /*if (id == R.id.action_refresh) {
            invalidateData();
            getSupportLoaderManager().restartLoader(FORECAST_LOADER_ID, null, this);
            return true;
        }

        if (id == R.id.action_map) {
            openLocationInMap();
            return true;
        }*/

        // COMPLETED (6) Launch SettingsActivity when the Settings option is clicked
        if (id == R.id.action_settings) {
            Intent startSettingsActivity = new Intent(this, SettingsActivity.class);
            startActivity(startSettingsActivity);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    }
