package nyc.artdecoders.smscontrol;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import nyc.artdecoders.smscontrol.data.SMSControlContract;

import nyc.artdecoders.smscontrol.databinding.ListItemTemplateBinding;

/**
 * Created by Dominik on 01.02.2018.
 */

public class ConfiguredListAdapter extends RecyclerView.Adapter<ConfiguredListAdapter.TemplateViewHolder> {

    private Context mContext;
    private Cursor mCursor;
    private ListItemTemplateBinding mLitb;

    public interface ListItemClickListener {
        void onListItemClick(long clickedItemIndex);
    }

    final private ListItemClickListener mOnClickListener;

    /**
     * Constructor using the context and the db cursor
     *
     * @param context the calling context/activity
     */
    public ConfiguredListAdapter(Context context, ListItemClickListener cl) {
        this.mContext = context;
        mOnClickListener = cl;
    }

    @Override
    public TemplateViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // Get the RecyclerView item layout
        LayoutInflater inflater = LayoutInflater.from(mContext);
        //View view = inflater.inflate(R.layout.list_item_template, parent, false);
        mLitb = ListItemTemplateBinding.inflate(inflater, parent, false);

        //return new TemplateViewHolder(view);
        return new TemplateViewHolder(mLitb.getRoot());
    }

    @Override
    public void onBindViewHolder(TemplateViewHolder holder, int position) {
        if (!mCursor.moveToPosition(position))
            return;

        long id = mCursor.getLong(mCursor.getColumnIndex(SMSControlContract.ConfiguredDeviceDeepEntry._ID));
        String name = mCursor.getString(mCursor.getColumnIndex(SMSControlContract.ConfiguredDeviceDeepEntry.COLUMN_NAME_DESCRIPTION));
        String vendor = mCursor.getString(mCursor.getColumnIndex(SMSControlContract.ConfiguredDeviceDeepEntry.COLUMN_NAME_PHONENUMBER));
        String type = mCursor.getString(mCursor.getColumnIndex(SMSControlContract.ConfiguredDeviceDeepEntry.COLUMN_NAME_TYPE_ID));

        /*mLitb.deviceTextView.setText(name);
        mLitb.ivType.setImageResource(R.drawable.ic_security_black_24dp);
        mLitb.vendorTextView.setText(String.valueOf(vendor));*/
        /*holder.ivType.setImageResource(R.drawable.ic_security_black_24dp);
        holder.deviceNameTextView.setText(name);
        holder.vendorTextView.setText(String.valueOf(vendor));*/

        ListItemDevice lid = new ListItemDevice(id, name, vendor, type);
        mLitb.setDevice(lid);

        holder.itemView.setTag(id);
    }

    @Override
    public int getItemCount() {

        if (mCursor == null) {
            return 0;
        }
        return mCursor.getCount();
    }

    public Cursor swapCursor(Cursor c) {
        if (mCursor == c) {
            return null; // bc nothing has changed
        }
        Cursor temp = mCursor;
        this.mCursor = c;

        if (c != null) {
            this.notifyDataSetChanged();
        }
        return temp;
    }

    /**
     * Inner class to hold the views needed to display a single item in the recycler-view
     */
    class TemplateViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        // Will display the guest name
        /*TextView deviceNameTextView;
        // Will display the party size number
        TextView vendorTextView;
        AppCompatImageView ivType;*/

        /**
         * Constructor for our ViewHolder. Within this constructor, we get a reference to our
         * TextViews
         *
         * @param itemView The View that you inflated in
         *                 {@link ConfiguredListAdapter#onCreateViewHolder(ViewGroup, int)}
         */
        public TemplateViewHolder(View itemView) {
            super(itemView);

           /* deviceNameTextView = (TextView) itemView.findViewById(R.id.device_text_view);
            vendorTextView = (TextView) itemView.findViewById(R.id.vendor_text_view);
            ivType = itemView.findViewById(R.id.ivType);*/

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            long id = (long) v.getTag();
            mOnClickListener.onListItemClick(id);
        }


    }
}