package nyc.artdecoders.smscontrol;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import nyc.artdecoders.smscontrol.R;
import nyc.artdecoders.smscontrol.data.SMSControlContract;

/**
 * Created by Dominik on 17.03.2018.
 */

public class ActionListAdapter extends RecyclerView.Adapter<ActionListAdapter.ActionViewHolder>  {

    private Context mContext;
    private Cursor mCursor;

    public interface ListItemClickListener {
        void onListItemClick(int clickedItemIndex, String command);
    }

    final private ListItemClickListener mOnClickListener;
    /**
     * Constructor using the context and the db cursor
     *
     * @param context the calling context/activity
     */
    public ActionListAdapter(Context context, ListItemClickListener cl) {
        this.mContext = context;
        mOnClickListener = cl;
    }

    @Override
    public ActionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // Get the RecyclerView item layout
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.list_item_action, parent, false);
        return new ActionViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ActionViewHolder holder, int position) {
        if (!mCursor.moveToPosition(position))
            return;

        int idIdx = mCursor.getColumnIndex(SMSControlContract.ActionsForConfiguredDeviceEntry.COLUMN_NAME_ACTION_ID);
        //int nameIdx = mCursor.getColumnIndex(SMSControlContract.ActionsForConfiguredDeviceEntry.COLUMN_NAME_ACTION_TRANSLATION_KEY);
        int nameIdx = mCursor.getColumnIndex(SMSControlContract.ActionsForConfiguredDeviceEntry.COLUMN_NAME_NAME);
        int commandIdx = mCursor.getColumnIndex(SMSControlContract.ActionsForConfiguredDeviceEntry.COLUMN_NAME_COMMAND);

        int id = mCursor.getInt(idIdx);
        String name = mCursor.getString(nameIdx);
        String vendor = mCursor.getString(commandIdx);

        holder.itemView.setTag(id);
        holder.actionNameTextView.setText(name);
        holder.commandTextView.setText(String.valueOf(vendor));
    }

    @Override
    public int getItemCount() {

        if (mCursor == null) {
            return 0;
        }
        return mCursor.getCount();
    }

    public Cursor swapCursor(Cursor c) {
        if (mCursor == c) {
            return null; // bc nothing has changed
        }
        Cursor temp = mCursor;
        this.mCursor = c;

        if (c != null) {
            this.notifyDataSetChanged();
        }
        return temp;
    }

    /**
     * Inner class to hold the views needed to display a single item in the recycler-view
     */
    class ActionViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener {

        // Will display the guest name
        TextView actionNameTextView;
        // Will display the party size number
        TextView commandTextView;

        /**
         * Constructor for our ViewHolder. Within this constructor, we get a reference to our
         * TextViews
         *
         * @param itemView The View that you inflated in
         *                 {@link ActionListAdapter#onCreateViewHolder(ViewGroup, int)}
         */
        public ActionViewHolder(View itemView) {
            super(itemView);
            actionNameTextView = (TextView) itemView.findViewById(R.id.action_text_view);
            commandTextView = (TextView) itemView.findViewById(R.id.command_text_view);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int id = (int) v.getTag();
            String command = ((TextView) v.findViewById(R.id.command_text_view)).getText().toString();
            mCursor.moveToPosition(0);
            mOnClickListener.onListItemClick(id, command);
        }


    }
}