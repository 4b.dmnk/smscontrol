package nyc.artdecoders.smscontrol;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import nyc.artdecoders.smscontrol.R;

import nyc.artdecoders.smscontrol.data.SMSControlContract;
import nyc.artdecoders.smscontrol.data.SMSControlDbHelper;

public class SelectTemplateActivity extends AppCompatActivity implements TemplateListAdapter.ListItemClickListener,
        SearchView.OnQueryTextListener,
        LoaderManager.LoaderCallbacks<Cursor> {

    private TemplateListAdapter mAdapter;

    private SQLiteDatabase mDb;
    private Toast mToast;
    private RecyclerView mRecyclerView;

    private String mCursorFilter;

    private static final int ID_DEVICE_LOADER = 21;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_template);

        mRecyclerView = (RecyclerView) this.findViewById(R.id.all_templates_list_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        mRecyclerView.setHasFixedSize(true);

        /*SMSControlDbHelper dbHelper = new SMSControlDbHelper(this);
        mDb = dbHelper.getWritableDatabase();
        //dbHelper.onUpgrade(mDb, 1,1);
        Cursor cursor = getAllTemplates();*/

        mAdapter = new TemplateListAdapter(this, this);

        mRecyclerView.setAdapter(mAdapter);

        getSupportLoaderManager().initLoader(ID_DEVICE_LOADER, null, this);

    }

   /* private Cursor getAllTemplates() {
        // COMPLETED (6) Inside, call query on mDb passing in the table name and projection String [] order by COLUMN_TIMESTAMP
        return mDb.query(
                SMSControlContract.DeviceEntry.TABLE_NAME,
                null,
                null,
                null,
                null,
                null,
                SMSControlContract.DeviceEntry.COLUMN_NAME_TYPE_ID
        );
    }*/

    @Override
    public void onListItemClick(int clickedItemIndex) {
        if(mToast != null) {
            mToast.cancel();
        }


        /*String toastMessage = "Item #" + clickedItemIndex + " clicked.";
        mToast = Toast.makeText(this, toastMessage, Toast.LENGTH_LONG);
        mToast.show();*/



        Intent i = new Intent(this, ConfigureDeviceActivity.class);
        //i.putExtra(Intent.EXTRA_INDEX, clickedItemIndex);
        Uri uri = SMSControlContract.DeviceEntry.CONTENT_URI.buildUpon()
                .appendPath(Long.toString(clickedItemIndex)).build();
        i.setData(uri);
        //startActivity(i);
        startActivityForResult(i, 0); //TODO: name a request code
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        /* Use AppCompatActivity's method getMenuInflater to get a handle on the menu inflater */
        MenuInflater inflater = getMenuInflater();
        /* Use the inflater's inflate method to inflate our menu layout to this menu */
        inflater.inflate(R.menu.search, menu);

        final MenuItem searchItem = menu.findItem(R.id.action_search);
        final SearchView searchView = new SearchView(SelectTemplateActivity.this); //(SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(this);
        searchItem.setActionView(searchView);
        /* Return true so that the menu is displayed in the Toolbar */
        return true;
    }


    @Override
    public boolean onQueryTextChange(String query) {
        // Here is where we are going to implement the filter logic
        mCursorFilter = !TextUtils.isEmpty(query) ? query : null;
        getSupportLoaderManager().restartLoader(ID_DEVICE_LOADER, null, this);
        return true;
    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == 0) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                // The user picked a contact.
                // The Intent's data Uri identifies which contact was selected.

                // Do something with the contact here (bigger example below)
                setResult(0);
                finish();
            }
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int loaderId, Bundle args) {

        switch (loaderId) {
            case ID_DEVICE_LOADER:
                Uri forecastQueryUri = SMSControlContract.DeviceEntry.CONTENT_URI;
                //String sortOrder = PoetcomContract.MachineEntry.COLUMN_PHKEY + " ASC";
                if(mCursorFilter != null) {
                    forecastQueryUri = Uri.withAppendedPath(SMSControlContract.DeviceEntry.CONTENT_FILTER_URI,
                            Uri.encode(mCursorFilter));
                }
                //String sortOrder = SMSControlContract.DeviceEntry.COLUMN_DATE + " ASC";
                //String selection = SMSControlContract.WeatherEntry.getSqlSelectForTodayOnwards();
                String sortOrder = SMSControlContract.DeviceEntry.COLUMN_NAME_TYPE_ID + " ASC, " +
                        SMSControlContract.DeviceEntry.COLUMN_NAME_PRODUCER + " ASC, " +
                        SMSControlContract.DeviceEntry.COLUMN_NAME_NAME + " ASC ";

                return new CursorLoader(this,
                        forecastQueryUri,
                        null,
                        null,
                        null,
                        sortOrder);

            default:
                throw new RuntimeException("Loader Not Implemented: " + loaderId);
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mAdapter.swapCursor(data);
        //if (mPosition == RecyclerView.NO_POSITION) mPosition = 0;
        //mRecyclerView.smoothScrollToPosition(mPosition);
       // if (data.getCount() != 0);// showWeatherDataView();
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.swapCursor(null);
    }
}
