package nyc.artdecoders.smscontrol;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.opengl.Visibility;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import nyc.artdecoders.smscontrol.R;

import de.hdodenhof.circleimageview.CircleImageView;
import nyc.artdecoders.smscontrol.data.SMSControlContract;

public class DeviceActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>,ActionListAdapter.ListItemClickListener {
    private static final String TAG = "DeviceActivity";
    Uri mUri;
    Long mDeviceId;

    private static final int MY_PERMISSIONS_REQUEST_SEND_SMS = 1;

    //device details
    private TextView mDescription;
    private TextView mPhoneNumber;
    private TextView mMake;
    private TextView mModel;
    private String mPassword;
    private String mTypeId;
    private ImageView mBackground;
    //private AppCompatImageView mIcon;
    private CircleImageView mIcon;
    private AppCompatImageView mIconSvg;

    //actions:
    private RecyclerView mRecyclerView;
    private ActionListAdapter mAdapter;

    //settings
    private boolean mUseIntent;

    //temp - between permission request and allow
    private String mPendingPhone;
    private String mPendingText;

    private static final int ID_CONFIGURED_DEVICE_DEEP_LOADER = 991;
    private static final int ID_ACTION_LOADER = 992;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device);

        mUri = getIntent().getData();
        if(mUri == null) {
            throw new NullPointerException("URI for Device Activity can't be null!");
        }

        mDeviceId = Long.valueOf(mUri.getLastPathSegment().toString());

        setupActionsRecyclerView();
        bindDeviceViews();


        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        mUseIntent = sharedPreferences.getBoolean(getString(R.string.pref_sendmethod_key), true);

        getSupportLoaderManager().initLoader(ID_CONFIGURED_DEVICE_DEEP_LOADER, null, this);
        getSupportLoaderManager().initLoader(ID_ACTION_LOADER, null, this);

    }

    private void bindDeviceViews() {
        mDescription = findViewById(R.id.tv_device_header);
        mPhoneNumber = findViewById(R.id.tv_phonenumber);
        mBackground = findViewById(R.id.ivBackground);
        mIcon = findViewById(R.id.ivIcon);
        mIconSvg = findViewById(R.id.ivIconSVG);
    }

    private void setupActionsRecyclerView() {
        mRecyclerView = this.findViewById(R.id.all_actions_list_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        mRecyclerView.setHasFixedSize(true);

        mAdapter = new ActionListAdapter(this, this);

        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int loaderId, Bundle args) {
        Uri queryUri;
        switch (loaderId){
            case ID_ACTION_LOADER:
                queryUri = SMSControlContract.ActionsForConfiguredDeviceEntry.CONTENT_URI.buildUpon()
                        .appendPath(Long.toString(mDeviceId)).build();

                return new CursorLoader(this,
                    queryUri,
                    null,
                    null,
                    null,
                    null);

            case ID_CONFIGURED_DEVICE_DEEP_LOADER:
                String id = mUri.getLastPathSegment();
                Uri uri = SMSControlContract.ConfiguredDeviceDeepEntry.CONTENT_URI.buildUpon().appendPath(id).build();

                return new CursorLoader(this,
                        uri,
                        null,
                        null,
                        null,
                        null);

            default:
                throw new RuntimeException("Loader Not Implemented: " + loaderId);
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        int loaderId = loader.getId();
        switch (loaderId){
            case ID_ACTION_LOADER:
                mAdapter.swapCursor(data);
                break;

            case ID_CONFIGURED_DEVICE_DEEP_LOADER:
                int deviceNameIdx = data.getColumnIndex(SMSControlContract.ConfiguredDeviceDeepEntry.COLUMN_NAME_DESCRIPTION);
                int makeIdx;
                //int modelIdx = data.getColumnIndex(SMSControlContract.DeviceEntry.)
                int phoneNumberIdx = data.getColumnIndex(SMSControlContract.ConfiguredDeviceDeepEntry.COLUMN_NAME_PHONENUMBER);
                int passwordIdx = data.getColumnIndex(SMSControlContract.ConfiguredDeviceDeepEntry.COLUMN_NAME_PASSWORD);
                int backgroundIdx = data.getColumnIndex(SMSControlContract.ConfiguredDeviceDeepEntry.COLUMN_NAME_CUSTOM_BACKGROUND);
                int iconIdx = data.getColumnIndex(SMSControlContract.ConfiguredDeviceDeepEntry.COLUMN_NAME_CUSTOM_IMAGE);
                int colorIdx = data.getColumnIndex(SMSControlContract.ConfiguredDeviceDeepEntry.COLUMN_NAME_COLOR);
                int typeIdIdx = data.getColumnIndex(SMSControlContract.ConfiguredDeviceDeepEntry.COLUMN_NAME_TYPE_ID);
//move to first
                    data.moveToFirst();
                mDescription.setText(data.getString(deviceNameIdx));
                mPhoneNumber.setText(data.getString(phoneNumberIdx));

                mPassword = data.getString(passwordIdx);
                mTypeId = data.getString(typeIdIdx);


                String path = data.getString(backgroundIdx);
                if(path != null && path.length() > 0) {
                    mBackground.setImageURI(Uri.parse(path));
                }

                path = data.getString(iconIdx);
                if(path != null && path.length() > 0) {
                    mIcon.setImageURI(Uri.parse(path));
                    mIcon.setVisibility(View.VISIBLE);
                    //Glide.with(getApplicationContext()).load(Uri.parse(path)).apply(new RequestOptions().centerCrop()).into((ImageView)findViewById(R.id.ivIcon));

                } else {
                    mIconSvg.setVisibility(View.VISIBLE);
                    switch (mTypeId) {
                        case "1":

                            mIconSvg.setImageResource(R.drawable.ic_security_black_24dp);
                            //.setImageURI(Uri.parse("android.resource://nyc.artdecoders.smscontrol/"+R.drawable.ic_security_black_24dp));
                           // mIcon.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_security_black_24dp, null ));
                            break;
                        case "2":

                            mIconSvg.setImageResource(R.drawable.ic_power_black_24dp);
                           //mIcon.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_power_black_24dp, null ));
                            break;
                        case "3":
                            mIconSvg.setImageResource(R.drawable.ic_ac_unit_black_24dp);
                            //mIcon.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_ac_unit_black_24dp, null ));
                            break;
                        case "4":
                            mIconSvg.setImageResource(R.drawable.ic_gps_fixed_black_24dp);
                           // mIcon.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_gps_fixed_black_24dp, null ));
                            break;
                        case "5":
                            mIconSvg.setImageResource(R.drawable.ic_videocam_black_24dp);
                           // mIcon.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_videocam_black_24dp, null ));
                            break;
                        default:
                            //mIcon.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_not_interested_black_24dp, null ));
                            // mIcon.setImageURI(
                            //Uri.parse("android.resource://nyc.artdecoders.smscontrol/"+R.drawable.ic_not_interested_black_24dp));

                            mIconSvg.setImageResource(R.drawable.ic_not_interested_black_24dp);
                            break;
                    }

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        mIcon.setImageTintList(getApplicationContext().getResources().getColorStateList(R.color.accent, null));
                    }
                }

                String color = data.getString(colorIdx);
                if(color != null && ( color.length() == 6 || color.length() == 7)) {
                    try {
                        color = color.length() == 6 ? "#"+color : color;
                        mDescription.setTextColor(Color.parseColor(color));
                    } catch (IllegalArgumentException ie) {
                        Log.e(TAG, "onLoadFinished: illegal color"+ ie.toString());
                    }
                }



                break;
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    @Override
    public void onListItemClick(int clickedItemIndex, String command) {
        String commandFilled = command.replace("__", mPassword);
        String phoneNumber = mPhoneNumber.getText().toString();
        commandFilled = phoneNumber + ":" + commandFilled;

        //Snackbar sb = Snackbar.make(findViewById(android.R.id.content), commandFilled, Snackbar.LENGTH_LONG);
        sendSMS(phoneNumber, commandFilled);

    }

    private void sendSMS(String phonenumber, String text) {
        if(!mUseIntent) { //preferences -> send directly
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.SEND_SMS)
                    != PackageManager.PERMISSION_GRANTED) {

                // Permission is not granted
                // Should we show an explanation?
                /*if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.SEND_SMS)) {
                    requestSMSPermission();
                    // Show an explanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.

                } else {

                    // No explanation needed; request the permission
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.SEND_SMS},
                            MY_PERMISSIONS_REQUEST_SEND_SMS);

                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                }*/
                mPendingPhone = phonenumber; mPendingText = text;
                requestSMSPermission();
            } else {
                // Permission has already been granted
                sendSMSSmsManager(phonenumber, text);
            }
        } else {
            sendSMSIntent(phonenumber, text);
        }

    }

    private void requestSMSPermission() {
        if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.SEND_SMS)) {
            Snackbar.make(findViewById(android.R.id.content), "SMS SEND Permission required", Snackbar.LENGTH_INDEFINITE).setAction("OK",
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ActivityCompat.requestPermissions(DeviceActivity.this,
                                    new String[]{Manifest.permission.SEND_SMS},
                                    MY_PERMISSIONS_REQUEST_SEND_SMS);
                        }
                    }).show();
        } else {
            Snackbar.make(findViewById(android.R.id.content), "cant send without permission", Snackbar.LENGTH_SHORT).show();
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.SEND_SMS}, MY_PERMISSIONS_REQUEST_SEND_SMS);
        }
    }

    private void sendSMSSmsManager(String phonenumber, String text) {
        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage(phonenumber, null, text, null, null);
    }

    private void sendSMSIntent(String phonenumber, String text) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:" + phonenumber));
        intent.putExtra("sms_body", text);
        startActivityForResult(intent, -1);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
        String permissions[], int[] grantResults) {
            switch (requestCode) {
                case MY_PERMISSIONS_REQUEST_SEND_SMS: {
                    // If request is cancelled, the result arrays are empty.
                    if (grantResults.length > 0
                            && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                        // permission was granted, yay! Do the
                        // contacts-related task you need to do.
                        sendSMSSmsManager(mPendingPhone, mPendingText);
                    } else {

                        // permission denied, boo! Disable the
                        // functionality that depends on this permission.
                    }
                    return;
                }

                // other 'case' lines to check for other
                // permissions this app might request.
            }
        }
}
