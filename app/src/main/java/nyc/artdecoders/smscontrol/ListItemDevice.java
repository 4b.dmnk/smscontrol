package nyc.artdecoders.smscontrol;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;

import nyc.artdecoders.smscontrol.R;

/**
 * Created by Dominik on 21.03.2018.
 */

public class ListItemDevice {
    private long id;
    private String name;
    private String vendor;
    private String type; // as resource identificator

    public ListItemDevice(long id, String name, String vendor, String type) {
        this.id = id;
        this.name = name;
        this.vendor = vendor;
        this.type = type;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public String getVendor() {
        return vendor;
    }

    public Uri getTypeImage() {
        int iconResource;
        if(type == null) {
            type = "0";
        }
        switch (type) {
            case "1":
                iconResource = R.drawable.ic_security_black_24dp;
                break;
            case "2":
                iconResource = R.drawable.ic_power_black_24dp;
                break;
            case "3":
                iconResource = R.drawable.ic_ac_unit_black_24dp;
                break;
            case "4":
                iconResource = R.drawable.ic_gps_fixed_black_24dp;
                break;
            case "5":
                iconResource = R.drawable.ic_videocam_black_24dp;
                break;
            default:
                iconResource = R.drawable.ic_not_interested_black_24dp;
                break;
        }


        return Uri.parse("android.resource://nyc.artdecoders.smscontrol/" + iconResource);
        //return Uri.parse("android.resource://"+c.getResources().getString(R.string.content_authority)+"/" + iconResource);
    }
}
